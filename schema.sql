--Apagando relacao
DROP TABLE IF EXISTS Carta;
DROP TABLE IF EXISTS Joga;
DROP TABLE IF EXISTS Vizinhos;
DROP TABLE IF EXISTS Conversa;
DROP TABLE IF EXISTS Amizade;
DROP TABLE IF EXISTS Participa_de_Grupo;

--Apagando as tabelas
DROP TABLE IF EXISTS Users;
DROP TABLE IF EXISTS Partida;
DROP TABLE IF EXISTS Territorio;
DROP  TABLE IF EXISTS Continente;
DROP TABLE IF EXISTS Grupo;
DROP TABLE IF EXISTS Objetivo;

-- Dominio
CREATE DOMAIN dominio_de_cor AS VARCHAR(20)
CONSTRAINT DOMINIO_DE_COR CHECK(
    VALUE ~ 'Amarelo' OR VALUE ~ 'Azul' OR  VALUE ~ 'Branco' OR VALUE ~ 'Preto' OR VALUE ~ 'Vermelho' OR VALUE ~ 'Verde'
);
--Criando as tabelas
CREATE TABLE Users (
    email VARCHAR(100) NOT NULL,
    username VARCHAR(40) NOT NULL UNIQUE ,
    senha VARCHAR(200) NOT NULL,
    CONSTRAINT PK_USER PRIMARY KEY (email)
    
);

CREATE TABLE Partida (
    cod_partida INT NOT NULL,
    data_inicio TIMESTAMP NOT NULL,
    data_fim TIMESTAMP,
    CONSTRAINT PK_PARTIDA PRIMARY KEY (cod_partida),
    
    user_email VARCHAR(100)
);

CREATE TABLE  Continente (
    nome VARCHAR(20) NOT NULL,
    bonus_de_tropa VARCHAR(100) NOT NULL,
    CONSTRAINT PK_CONTINENT PRIMARY KEY (nome)
);

CREATE TABLE Territorio (
    nome VARCHAR(20) NOT NULL,
    nome_continente VARCHAR(20) NOT NULL,
    CONSTRAINT FK_CONTINENT FOREIGN KEY (nome_continente) REFERENCES Continente(nome)
    ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT PK_TERRITORIO PRIMARY KEY (nome)
);

CREATE TABLE Ocupar (
    nome VARCHAR(20) NOT NULL REFERENCES Territorio,
    cod_partida INT ,
    user_email VARCHAR(100),
    quantidade_exercitos int not null,
    CONSTRAINT OCUPAR_FOREIGN_KEY FOREIGN KEY (cod_partida, user_email) REFERENCES Joga(cod_partida, user_email),
    PRIMARY KEY (nome, cod_partida, user_email)
);

CREATE TABLE Grupo (
    nome VARCHAR(40) NOT NULL,
    cod_grupo INT NOT NULL,
    CONSTRAINT PK_GRUPO PRIMARY KEY (cod_grupo)
);

CREATE TABLE Objetivo (
    nome VARCHAR(20) NOT NULL,
    descricao_primaria TEXT NOT NULL,
    descricao_secundaria TEXT,
    CONSTRAINT PK_OBJETIVO PRIMARY KEY (nome)
);

--Criando relaçoes
CREATE TABLE Joga (
    cod_partida INT NOT NULL REFERENCES Partida(cod_partida) ON UPDATE CASCADE ON DELETE RESTRICT,
    user_email VARCHAR(100) NOT NULL REFERENCES Users (email) ON UPDATE CASCADE ON DELETE RESTRICT,
    cod_partida_proximo INT ,
    user_email_proximo VARCHAR(100),
    cor_exercito dominio_de_cor,
    nome_objetivo VARCHAR(20) REFERENCES Objetivo(nome),
    CONSTRAINT PK_JOGA PRIMARY KEY (cod_partida, user_email),
    CONSTRAINT PROX_FOREIGN_KEY FOREIGN KEY (cod_partida_proximo, user_email_proximo) REFERENCES Joga(cod_partida, user_email)
    ON UPDATE CASCADE ON DELETE CASCADE
);
CREATE TABLE Vizinhos (
    territorio_1 VARCHAR(20) NOT NULL REFERENCES Territorio(nome) ON UPDATE CASCADE ON DELETE CASCADE, 
    territorio_2 VARCHAR(20) NOT NULL REFERENCES Territorio(nome) ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT PK_VIZINHOS PRIMARY KEY (territorio_1, territorio_2)
);
CREATE TABLE Conversa (
    horario TIMESTAMP NOT NULL,
    remetente VARCHAR(100) NOT NULL REFERENCES Users (email) ON UPDATE CASCADE ON DELETE RESTRICT,
    destinatario VARCHAR(100) NOT NULL REFERENCES Users (email) ON UPDATE CASCADE ON DELETE RESTRICT,
    mensagem TEXT NOT NULL,
    CONSTRAINT PK_CONVERSA PRIMARY KEY (horario, remetente, destinatario)
);

CREATE TABLE Amizade (
    remetente VARCHAR(100) NOT NULL REFERENCES Users (email) ON UPDATE CASCADE ON DELETE RESTRICT,
    destinatario VARCHAR(100) NOT NULL REFERENCES Users (email) ON UPDATE CASCADE ON DELETE RESTRICT,
    aceitou BOOLEAN NOT NULL,
    CONSTRAINT PK_AMIZADE PRIMARY KEY (remetente, destinatario)
);

CREATE TABLE Participa_de_Grupo (
    user_email VARCHAR(100) NOT NULL REFERENCES Users (email) ON UPDATE CASCADE ON DELETE RESTRICT,
    cod_grupo INT NOT NULL REFERENCES Grupo(cod_grupo),
    CONSTRAINT PK_PERT_GRUP PRIMARY KEY (user_email, cod_grupo)
);

CREATE TABLE Carta (
    territorio VARCHAR(20) NOT NULL REFERENCES Territorio(nome) ON UPDATE CASCADE ON DELETE CASCADE, 
    cod_partida INT,
    user_email VARCHAR(100),
    CONSTRAINT JOGO_FOREIGN_KEY FOREIGN KEY (cod_partida, user_email) REFERENCES Joga(cod_partida, user_email)
    ON UPDATE CASCADE ON DELETE CASCADE
);

