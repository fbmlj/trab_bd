--relacionado com usuario

INSERT INTO Users(email,username, senha) VALUES ('lucastavasousa@gmail.com','fbmlj', '123456');
INSERT INTO Users(email,username, senha) VALUES ('canellas@gmail.com','canellas_bd', '123456');

INSERT INTO Grupo(nome, cod_grupo) VALUES ('TRABALHO DE BD', 1);
INSERT INTO Participa_de_Grupo(user_email, cod_grupo) VALUES ('lucastavasousa@gmail.com', 1);

INSERT INTO Participa_de_Grupo(cod_grupo, user_email) VALUES (1,'canellas@gmail.com');

INSERT INTO Conversa (horario, remetente, destinatario, mensagem) VALUES ('2019-11-4 19:55:22', 'lucastavasousa@gmail.com', 'canellas@gmail.com', 'mensagem');
INSERT INTO Amizade (remetente, destinatario, aceitou) VALUES ('lucastavasousa@gmail.com', 'canellas@gmail.com', TRUE);


--JOGO
INSERT INTO Continente(nome, bonus_de_tropa) VALUES ('Europa', '+ 1 tropa por segundo');
INSERT INTO Territorio(nome, nome_continente) VALUES ('Portugal', 'Europa');
INSERT INTO Territorio(nome, nome_continente) VALUES ('Espanha', 'Europa');
INSERT INTO Territorio(nome, nome_continente) VALUES ('Londres', 'Europa');

INSERT INTO VIZINHOS VALUES ('Portugal', 'Espanha');

INSERT INTO Objetivo VALUES ('destruir verde', 'seu exercito precisa finalizar o jogador de cor_exercito verde','coquistar europa');
INSERT INTO Partida(cod_partida, data_inicio, data_fim) VALUES (1, '2019-10-4 19:55:22', NULL);

INSERT INTO JOGA(cod_partida, user_email, cod_partida_proximo, user_email_proximo, nome_objetivo, cor_exercito) VALUES (1, 'lucastavasousa@gmail.com', 1, NULL, 'destruir verde', 'verde');

INSERT INTO JOGA(cod_partida, user_email, cod_partida_proximo, user_email_proximo, nome_objetivo, cor_exercito) VALUES (1, 'canellas@gmail.com', 1, 'lucastavasousa@gmail.com', 'destruir verde', 'Azul');

INSERT INTO Partida(cod_partida, data_inicio, data_fim) VALUES (2, '2019-10-4 19:55:22', '2019-10-4 19:58:22');
INSERT INTO JOGA(cod_partida, user_email, cod_partida_proximo, user_email_proximo, nome_objetivo, cor_exercito) VALUES (2, 'canellas@gmail.com', 2, NULL, 'destruir verde', 'Azul');
